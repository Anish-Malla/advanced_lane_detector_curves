# Advanced Lane Detection [Python3 OpenCV]

- Compared to the previous lane detection this one is more advanced, main reason being we calculate the curve and not just straight lines, here's the basic steps:

1. Undistort the image, most modern day cameras, distort the image slightly, and we want to get rid of that effect.
2. Warp the images, in other words look at it from a top down view or birds eye view of a specific ROI (predefined trapezoidal area). Makes it easier to find the lane lines.
3. We get a binary threshold using color filters, looking for the colors yellow and white and we also apply an edge detector.
4. Apply the filter on the image and only focus on the parts that will most probably be a line, with these points use the sliding window algorithm and just focus on the points where the maximum "detected lanes" are there.
5. Fit a curve around these points
6. Draw and display this curve on the final image and show it
